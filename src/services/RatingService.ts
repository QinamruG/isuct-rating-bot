import RatingSubject from "../models/SubjectRatingModel";
import fetch from "node-fetch";
import { HTMLElement, parse } from "node-html-parser";

export function GetFormattedSubjectRating(subj: RatingSubject) {
  return `Семестр: ${subj.Semester}\nПредмет: ${subj.SubjectName}\nИтоговый рейтинг: ${subj.FinalScore}\nК.Т.1: ${subj.FirstCheckPointScore}    Прогулы К.Т.1: ${subj.NumberOfAbsenteeismAtTheFirstCheckpoint}\nК.Т.2: ${subj.SecondCheckPointScore}    Прогулы К.Т.2: ${subj.NumberOfAbsenteeismAtTheSecondCheckpoint}\nК.Т.3: ${subj.ThirdCheckPointScore}    Прогулы К.Т.3: ${subj.NumberOfAbsenteeismAtTheThirdCheckpoint}`;
}

function createRatingSubjectFromNode(node: HTMLElement) {
  const cNodes = node.childNodes;
  const subject: RatingSubject = {
    Semester: Number(cNodes[0].innerText),
    SubjectName: cNodes[1].innerText,
    Type: cNodes[2].innerText,
    FinalScore: Number(cNodes[3].innerText),
    Term: cNodes[4].innerText,
    FirstCheckPointScore: cNodes[5].innerText,
    NumberOfAbsenteeismAtTheFirstCheckpoint: Number(cNodes[6].innerText),
    SecondCheckPointScore: cNodes[7].innerText,
    NumberOfAbsenteeismAtTheSecondCheckpoint: Number(cNodes[8].innerText),
    ThirdCheckPointScore: cNodes[9].innerText,
    NumberOfAbsenteeismAtTheThirdCheckpoint: Number(cNodes[10].innerText),
  };

  return subject;
}

async function getRatingPageHtml(studnumber: number, paspnumber: number) {
  try {
    const url = `https://www.isuct.ru/student/rating/view?paspnumber=${paspnumber}&studnumber=${studnumber}`;
    const response = await fetch(url);
    const html = await response.text();
    return html;
  } catch (err) {
    console.error(err);
    throw err;
  }
}

async function getRatingNodes(studnumber: number, paspnumber: number) {
  const html = await getRatingPageHtml(studnumber, paspnumber);

  const parsedHtml = parse(html);
  const nodeCollection = parsedHtml.querySelectorAll("tr");

  return nodeCollection;
}

export async function GetFullRating(studnumber: number, paspnumber: number) {
  const content = [];
  try {
    const nodeCollection = await getRatingNodes(studnumber, paspnumber);

    if (!nodeCollection.length) return null;

    for (const node of nodeCollection)
      content.push(createRatingSubjectFromNode(node));

    content.shift();

    return content;
  } catch (err) {
    console.error(err);
    return null;
  }
}
