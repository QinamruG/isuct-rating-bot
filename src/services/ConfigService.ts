import { config } from "dotenv";

class ConfigFactory {
  public readonly TELEGRAM_BOT_TOKEN: string;
  public readonly SERVER_PORT: number;
  public readonly IS_DEVELOPMENT: boolean;
  public readonly TELEGRAM_WEBHOOK_URL: string;
  // public readonly TELEGRAM_SECRET_PATH: string;

  constructor(envPath?: string) {
    config({ path: envPath });

    this.IS_DEVELOPMENT = process.env.NODE_ENV === "development";

    const tgWebhookUrl = process.env.TELEGRAM_BOT_WEBHOOK_URL;
    if (!tgWebhookUrl) throw new Error("Telegram webhook url must be provided");
    this.TELEGRAM_WEBHOOK_URL = tgWebhookUrl;

    // const tgSecretPath = process.env.TELEGRAM_SECRET_PATH;
    // if (!tgSecretPath) throw new Error("Telegram webhook url must be provided");
    // this.TELEGRAM_SECRET_PATH = tgSecretPath;

    const serverPort = process.env.SERVER_PORT;
    if (!serverPort) throw new Error("Server port must be provided");
    this.SERVER_PORT = Number(serverPort);

    const telegramBotToken = process.env.TELEGRAM_BOT_TOKEN;
    if (!telegramBotToken)
      throw new Error("Telegram bot token must be provided");
    this.TELEGRAM_BOT_TOKEN = telegramBotToken;

    if (this.IS_DEVELOPMENT) {
      console.log("Env configuration variables", this);
    }
  }
}

export const configInstance = new ConfigFactory();
