import { Telegraf, Scenes, session } from "telegraf";
import {
  GetFullRating,
  GetFormattedSubjectRating,
} from "./services/RatingService";
import { configInstance } from "./services/ConfigService";

const RatingScene = new Scenes.WizardScene(
  "rating-scene",
  async (ctx) => {
    ctx.wizard.state.data = {};
    ctx.reply("введите ваш студенческий");
    return ctx.wizard.next();
  },
  async (ctx) => {
    ctx.wizard.state.data.studnum = Number(ctx.message.text);
    ctx.reply(
      "введите номер паспорта (того, с которым поступили в университет) "
    );
    return ctx.wizard.next();
  },
  async (ctx) => {
    ctx.wizard.state.data.paspnum = Number(ctx.message.text);
    ctx.wizard.state.data.rating = await GetFullRating(
      ctx.wizard.state.data.studnum,
      ctx.wizard.state.data.paspnum
    );
    if (!ctx.wizard.state.data.rating) {
      ctx.reply(
        "К сожалению, ничего не найдено. Перепроверьте введенные данные и попробуйте снова"
      );
      return ctx.scene.leave();
    }
    ctx.reply("введите номер семестра");
    return ctx.wizard.next();
  },
  async (ctx) => {
    ctx.wizard.state.data.semnum = Number(ctx.message.text);
    ctx.wizard.state.data.rating.forEach((subject) => {
      if (subject.Semester === ctx.wizard.state.data.semnum)
        ctx.reply(GetFormattedSubjectRating(subject));
    });
    return ctx.scene.leave();
  }
);

const stage = new Scenes.Stage([RatingScene]);
const bot = new Telegraf(configInstance.TELEGRAM_BOT_TOKEN);
bot.use(session());
bot.use(stage.middleware());

bot.start(async (ctx) => {
  ctx.reply(
    `Привет, ${ctx.message.from.first_name}! Этот бот может показать вам ваш рейтинг студента. Для этого вам нужен ваши номера студенческого билета и паспорта.\nПриятного использования!`
  );
});

bot.help(async (ctx) =>
  ctx.reply(
    "Узнайте свой рейтинг (или не свой)!\nВведите номер студенческого билета и номер паспорта, без серии.\nОбещаем не продавать ваши данные =)\nдоступные команды: /rating - узнать рейтинг"
  )
);

bot.command("rating", async (ctx) => {
  ctx.scene.enter("rating-scene");
});

bot.launch();

process.once("SIGINT", () => bot.stop("SIGINT"));
process.once("SIGTERM", () => bot.stop("SIGTERM"));
