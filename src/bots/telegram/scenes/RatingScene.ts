import { userInfo } from "os";
import { Context, Scenes } from "telegraf";
import RatingSubject from "../../../models/SubjectRatingModel";
import {
  GetFormattedSubjectRating,
  GetFullRating,
} from "../../../services/RatingService";

type User = {
  studNum: number;
  paspNum: number;
};

type RatingState = {
  semesterNum: number;
  user: User;
  rating: RatingSubject[] | null;
};

export interface RatingSceneContext extends Context {
  semester: number;
  scene: Scenes.SceneContextScene<RatingSceneContext, Scenes.WizardSessionData>;
  wizard: Scenes.WizardContextWizard<RatingSceneContext>;
  message: any;
  rating: any;
  state: RatingState;
}

export const RatingScene = new Scenes.WizardScene<RatingSceneContext>(
  "rating-scene",
  async (ctx) => {
    (ctx.wizard.state as any).user = {};
    ctx.reply("введите ваш студенческий");
    return ctx.wizard.next();
  },
  async (ctx) => {
    const studNum = parseInt(ctx.message.text);
    console.log(studNum);
    (ctx.wizard.state as any).user.studNum = studNum;
    ctx.reply(
      "введите номер паспорта (того, с которым поступили в университет) "
    );
    return ctx.wizard.next();
  },
  async (ctx) => {
    (ctx.wizard.state as any).user.paspNum = Number(ctx.message.text);
    (ctx.wizard.state as any).rating = await GetFullRating(
      (ctx.wizard.state as any).user.studNum,
      (ctx.wizard.state as any).user.paspNum
    );
    console.log(ctx.wizard.state);
    if (!(ctx.wizard.state as any).rating) {
      ctx.reply(
        "К сожалению, ничего не найдено. Перепроверьте введенные данные и попробуйте снова"
      );
      return ctx.scene.leave();
    }
    ctx.reply("введите номер семестра");
    return ctx.wizard.next();
  },
  async (ctx) => {
    const semNum = Number(ctx.message.text);
    (ctx.wizard.state as any).rating!.forEach((subject: any) => {
      if (subject.Semester === semNum)
        ctx.reply(GetFormattedSubjectRating(subject));
    });
    return ctx.scene.leave();
  }
);
