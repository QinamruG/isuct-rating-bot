import { Scenes, session, Telegraf } from "telegraf";
import { RatingScene, RatingSceneContext } from "./scenes/RatingScene";
import { configInstance } from "../../services/ConfigService";

const stage = new Scenes.Stage([RatingScene]);
const telegramBot = new Telegraf<RatingSceneContext>(
  configInstance.TELEGRAM_BOT_TOKEN
);
telegramBot.use(session());
telegramBot.use(stage);

telegramBot.start(async (ctx) => {
  ctx.reply(
    `Привет, ${ctx.message.from.first_name}! Этот бот может показать вам ваш рейтинг студента. Для этого вам нужен ваши номера студенческого билета и паспорта.\nПриятного использования!`
  );
});

telegramBot.command("rating", async (ctx) => {
  ctx.scene.enter("rating-scene");
});

export { telegramBot };
