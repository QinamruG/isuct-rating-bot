import fastify from "fastify";
import { Telegraf } from "telegraf";
import { configInstance } from "./services/ConfigService";
import telegrafPlugin from "fastify-telegraf";
import { telegramBot } from "./bots";

const app = fastify({ logger: true });

const SECRET_PATH = `/telegraf/${telegramBot.secretPathComponent()}`;

app.register(telegrafPlugin, { bot: telegramBot, path: SECRET_PATH });

app.get("/", async (request, reply) => {
  return { hello: "world" };
});

telegramBot.telegram
  .setWebhook(configInstance.TELEGRAM_WEBHOOK_URL + SECRET_PATH)
  .then(() => {
    console.log(
      "Webhook for telegram is set on",
      configInstance.TELEGRAM_WEBHOOK_URL
    );
  });

const start = async () => {
  try {
    app.listen(configInstance.SERVER_PORT, () => {
      console.log("I am alive at port " + configInstance.SERVER_PORT);
    });
  } catch (err) {
    app.log.error(err);
    process.exit(1);
  }
};
start();
