export default interface RatingSubject {
    Semester: number; // номер семестра
    SubjectName: string; // название предмета
    Type: string; // Тип
    FinalScore: number; // Итоговый балл
    Term: string; // срок
    FirstCheckPointScore: string; // баллы 1-й КТ
    SecondCheckPointScore: string; // баллы 2-й КТ
    ThirdCheckPointScore: string; // баллы 3-й КТ
    NumberOfAbsenteeismAtTheFirstCheckpoint: number; // прогулы в 1-й КТ
    NumberOfAbsenteeismAtTheSecondCheckpoint: number; // прогулы во 2-й КТ
    NumberOfAbsenteeismAtTheThirdCheckpoint: number; // прогулы в 3-й КТ
}
